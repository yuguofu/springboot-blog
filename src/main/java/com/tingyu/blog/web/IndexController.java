package com.tingyu.blog.web;

import com.tingyu.blog.MyNotFoundException;
import com.tingyu.blog.service.BlogService;
import com.tingyu.blog.service.TagService;
import com.tingyu.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

    @Autowired
    private BlogService blogService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private TagService tagService;

    //首页
    @GetMapping("/")
    public String index(@PageableDefault(size = 3,sort = {"updateTime"},direction = Sort.Direction.DESC) Pageable pageable, Model model){
        model.addAttribute("page",blogService.getBlogs(pageable));
        model.addAttribute("types",typeService.listTypeTop(6));
        model.addAttribute("tags",tagService.listTagTop(10));
        model.addAttribute("recommendBlogs",blogService.listRecommendBlogTop(6));
        return "index";
    }

    //搜索
    @PostMapping("/search")
    public String search(@PageableDefault(size = 3,sort = {"updateTime"},direction = Sort.Direction.DESC) Pageable pageable, @RequestParam String query , Model model){
        model.addAttribute("page",blogService.searchBlogs("%"+query+"%",pageable));
        model.addAttribute("query",query);
        return "search";
    }


    //博客详情
    @GetMapping("/blog/{id}")
    public String blog(@PathVariable int id,Model model){
        model.addAttribute("blog",blogService.getAndConvert(id));
        return "blog";
    }
}
