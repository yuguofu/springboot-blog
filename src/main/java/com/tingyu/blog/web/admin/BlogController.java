package com.tingyu.blog.web.admin;


import com.tingyu.blog.po.Blog;
import com.tingyu.blog.po.User;
import com.tingyu.blog.service.BlogService;
import com.tingyu.blog.service.TagService;
import com.tingyu.blog.service.TypeService;
import com.tingyu.blog.vo.BlogQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class BlogController {
    //注入BlogService
    @Autowired
    private BlogService blogService;
    //注入TypeService
    @Autowired
    private TypeService typeService;
    //注入TagService
    @Autowired
    private TagService tagService;

//    //博客列表
//    @GetMapping("/blogManage")
//    public String blogManager(@PageableDefault(size = 3,sort = {"updateTime"},direction = Sort.Direction.DESC) Pageable pageable, BlogQuery blog, Model model){
//        model.addAttribute("types",typeService.listType());
//        model.addAttribute("page",blogService.listBlog(pageable,blog));
//        return "/admin/blogManage";
//    }

    //博客列表
    @GetMapping("/blogManage")
    public String blogManager(@PageableDefault(size = 10,sort = {"updateTime"},direction = Sort.Direction.DESC) Pageable pageable, Model model){
        model.addAttribute("types",typeService.listType());
        model.addAttribute("page",blogService.getBlogs(pageable));
        return "/admin/blogManage";
    }


    //搜索
    @PostMapping("/blogManage/search")
    public String search(@PageableDefault(size = 10,sort = {"updateTime"},direction = Sort.Direction.DESC) Pageable pageable, BlogQuery blogQuery, Model model){
        model.addAttribute("page",blogService.listBlog(pageable,blogQuery));
        return "/admin/blogManage :: blogList";
    }

    //打开发布页
    @GetMapping("/blogPublish")
    public String blogPublish(Model model){
        //获取到所有分类传到页面
        model.addAttribute("types",typeService.listType());
        //获取到所有标签传到页面
        model.addAttribute("tags",tagService.listTag());
        model.addAttribute("blog",new Blog());
        return "/admin/blogPublish";
    }

    //post提交添加博客
    @PostMapping("/blogPublish")
    public String postPublish(Blog blog, RedirectAttributes attributes, HttpSession session){
        blog.setUser((User) session.getAttribute("user"));
        blog.setType(typeService.getOneType(blog.getType().getId()));
        blog.setTags(tagService.listTag(blog.getTagIds()));
        Blog b = blogService.saveBlog(blog);
        if (b == null){     //保存失败
            attributes.addFlashAttribute("message","添加失败！");
        }else {     //保存成功
            attributes.addFlashAttribute("message","添加成功！");
        }
        return "redirect:/admin/blogManage";
    }


    //打开博客编辑页
    @GetMapping("/blogManage/{id}/edit")
    public String blogEdit(@PathVariable int id, Model model){
        //获取到所有分类传到页面
        model.addAttribute("types",typeService.listType());
        //获取到所有标签传到页面
        model.addAttribute("tags",tagService.listTag());
        //获取博客信息
        Blog b = blogService.getBlog(id);
        b.init();
        model.addAttribute("blog",b);
        return "/admin/blogEdit";
    }

    //post提交编辑修改博客
    @PostMapping("/blogEdit")
    public String postEdit(Blog blog, RedirectAttributes attributes, HttpSession session){
        blog.setUser((User) session.getAttribute("user"));
        blog.setType(typeService.getOneType(blog.getType().getId()));
        blog.setTags(tagService.listTag(blog.getTagIds()));
        Blog b = blogService.updateBlog(blog.getId(),blog);
        if (b == null){     //修改失败
            attributes.addFlashAttribute("message","编辑失败！");
        }else {     //修改成功
            attributes.addFlashAttribute("message","编辑成功！");
        }
        return "redirect:/admin/blogManage";
    }

    //删除博客
    @GetMapping("/blogManage/{id}/delete")
    public String deleteBlog(@PathVariable int id,RedirectAttributes attributes){
        blogService.deleteBlog(id);
        attributes.addFlashAttribute("message","删除成功！");
        return "redirect:/admin/blogManage";
    }

}
