package com.tingyu.blog.web.admin;


import com.tingyu.blog.po.Tag;
import com.tingyu.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class TagController {
    @Autowired
    private TagService tagService;

    //get请求标签管理页
    @GetMapping("/tagManage")
    public String tagManage(@PageableDefault(size = 10,sort = {"id"},direction = Sort.Direction.DESC) Pageable pageable, Model model){
        model.addAttribute("page",tagService.listTag(pageable));
        return "/admin/tagManage";
    }

    //get请求标签添加页
    @GetMapping("/addTag")
    public String addTag(Model model){
        model.addAttribute("tag",new Tag());
        return "/admin/addTag";
    }

    //post提交,添加标签
    @PostMapping("/addTag")
    public String post(@Valid Tag tag, BindingResult result, RedirectAttributes attributes){
        //根据name判断对象是否存在
        Tag tag1 = tagService.getOneTagByName(tag.getName());
        if (tag1 != null){
            result.rejectValue("name","nameError","不能重复添加！");
        }

        if (result.hasErrors()){
            return "/admin/addTag";
        }
        Tag t = tagService.saveTag(tag);
        if (t == null){     //保存失败
            attributes.addFlashAttribute("message","添加失败！");
        }else {     //保存成功
            attributes.addFlashAttribute("message","添加成功！");
        }
        return "redirect:/admin/tagManage";    //重定向到标签管理页
    }

    //编辑标签
    @GetMapping("/tagManage/{id}/edit")
    public String editTag(@PathVariable int id , Model model){
        model.addAttribute("tag",tagService.getOneTag(id));
        return "/admin/editTag";
    }

    //post提交,修改标签
    @PostMapping("/editTag/{id}")
    public String editPost(@Valid Tag tag, BindingResult result,@PathVariable int id, RedirectAttributes attributes){
        //根据name判断对象是否存在
        Tag tag1 = tagService.getOneTagByName(tag.getName());
        if (tag1 != null){
            result.rejectValue("name","nameError","不能重复添加！");
        }
        //实体校验
        if (result.hasErrors()){
            return "/admin/editTag";
        }
        Tag t = tagService.editTag(id,tag);
        if (t == null){     //修改失败
            attributes.addFlashAttribute("message","修改失败！");
        }else {     //修改成功
            attributes.addFlashAttribute("message","修改成功！");
        }
        return "redirect:/admin/tagManage";    //重定向到标签管理页
    }

    //删除标签
    @GetMapping("/tagManage/{id}/delete")
    public String delete(@PathVariable int id,RedirectAttributes attributes){
        tagService.deleteTag(id);
        attributes.addFlashAttribute("message","删除成功！");
        return "redirect:/admin/tagManage";
    }
}
