package com.tingyu.blog.web.admin;


import com.tingyu.blog.po.Type;
import com.tingyu.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class TypeController {
    @Autowired
    private TypeService typeService;

    @GetMapping("/typeManage")
    public String typeManage(@PageableDefault(size = 10,sort = {"id"},direction = Sort.Direction.DESC) Pageable pageable, Model model){
        model.addAttribute("page",typeService.listType(pageable));
        return "/admin/typeManage";
    }

    @GetMapping("/addType")
    public String addType(Model model){
        model.addAttribute("type",new Type());
        return "/admin/addType";
    }

    //post提交,添加分类
    @PostMapping("/addType")
    public String post(@Valid Type type,BindingResult result, RedirectAttributes attributes){
        //根据name判断对象是否存在
        Type type1 = typeService.getOneTypeByName(type.getName());
        if (type1 != null){
            result.rejectValue("name","nameError","不能重复添加！");
        }

        if (result.hasErrors()){
            return "/admin/addType";
        }
        Type t = typeService.saveType(type);
        if (t == null){     //保存失败
            attributes.addFlashAttribute("message","添加失败！");
        }else {     //保存成功
            attributes.addFlashAttribute("message","添加成功！");
        }
        return "redirect:/admin/typeManage";    //重定向到分类管理页
    }

    //编辑分类
    @GetMapping("/typeManage/{id}/edit")
    public String editType(@PathVariable int id , Model model){
        model.addAttribute("type",typeService.getOneType(id));
        return "/admin/editType";
    }

    //post提交,修改分类
    @PostMapping("/editType/{id}")
    public String editPost(@Valid Type type,BindingResult result,@PathVariable int id, RedirectAttributes attributes){
        //根据name判断对象是否存在
        Type type1 = typeService.getOneTypeByName(type.getName());
        if (type1 != null){
            result.rejectValue("name","nameError","不能重复添加！");
        }
        //实体校验
        if (result.hasErrors()){
            return "/admin/editType";
        }
        Type t = typeService.editType(id,type);
        if (t == null){     //修改失败
            attributes.addFlashAttribute("message","修改失败！");
        }else {     //修改成功
            attributes.addFlashAttribute("message","修改成功！");
        }
        return "redirect:/admin/typeManage";    //重定向到分类管理页
    }

    //删除分类
    @GetMapping("/typeManage/{id}/delete")
    public String delete(@PathVariable int id,RedirectAttributes attributes){
        typeService.deleteType(id);
        attributes.addFlashAttribute("message","删除成功！");
        return "redirect:/admin/typeManage";
    }
}
