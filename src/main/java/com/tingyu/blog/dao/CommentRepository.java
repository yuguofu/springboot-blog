package com.tingyu.blog.dao;

import com.tingyu.blog.po.Comment;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment,Integer> {

    //根据博客id获取父评论为空的评论集合
    List<Comment> findByBlogIdAndParentCommentNull(int id, Sort sort);
}
