package com.tingyu.blog.service;

import com.tingyu.blog.po.Blog;
import com.tingyu.blog.vo.BlogQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface BlogService {

    Blog getBlog(int id);   //获取一个博客

    Blog getAndConvert(int id);     //获取并转换博客内容

    Page<Blog> listBlog(Pageable pageable, BlogQuery blogQuery);   //多条件查询博客集合

    Page<Blog> listBlog(int tagId, Pageable pageable);  //根据标签id查询博客

    Map<String,List<Blog>> archiveBlog();   //按年份查询博客

    Long countBlog();    //获取博客总数

    Page<Blog> getBlogs(Pageable pageable);     //查询所有博客

    List<Blog> listRecommendBlogTop(int size);   //最新推荐

    Page<Blog> searchBlogs(String query, Pageable pageable);    //搜索博客

    Blog saveBlog(Blog blog);   //添加一个博客

    Blog updateBlog(int id , Blog blog);    //更新（修改）博客

    void deleteBlog(int id);    //删除博客
}
