package com.tingyu.blog.service;

import com.tingyu.blog.po.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TagService {

    Tag saveTag(Tag tag);   //保存添加的标签

    Tag getOneTag(int id);    //通过id获取一个标签

    List<Tag> listTag();    //获取所有标签

    List<Tag> listTagTop(int size);     //获取前列表前几条

    List<Tag> listTag(String ids);    //获取选中的标签

    Tag getOneTagByName(String name);     //通过名称获取一个标签对象

    Page<Tag> listTag(Pageable pageable);     //分页查询

    Tag editTag(int id, Tag tag);    //编辑标签

    void deleteTag(int id);    //删除标签
}
